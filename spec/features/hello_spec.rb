require 'rails_helper'
RSpec.describe HelloController do
  it 'returns "Hello World!" on root' do
    visit root_path
    expect(page).to have_content 'Hello World!'
  end

  it 'does not contain a second heading' do
    visit root_path
    expect(page).to have_content 'Hello World!'
    expect(page).not_to have_content 'How is it going?'
  end

  context '/hello' do
    it 'returns the name when specified' do
      visit hello_path 'tester'
      expect(page).to have_content 'Hello Tester!'
    end

    it 'returns "Hello World!" when only whitespaces were specified' do
      visit hello_path ' '
      expect(page).to have_content 'Hello World!'
    end

    it 'contains a second heading' do
      visit hello_path 'tester'
      expect(page).to have_content 'Hello Tester!'
      expect(page).to have_content 'How is it going?'
    end
  end
end
